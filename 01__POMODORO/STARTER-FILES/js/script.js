function pomodoro() {
  window.interv = setInterval( () => {
    let seconds = $(".seconds").find("input").val();  // string
    let minutes = $(".minutes").find("input").val();  // string
    let nsec = Number(seconds) - 1;
    let nmin = Number(minutes);
    // manage transitions, for example 01:00 to 00:59
    if (nsec < 0) {
      nsec = 60 + nsec;
      nmin = nmin - 1;
    }
    // display correctly padded seconds and minutes
    $(".seconds").find("input").val(nsec.toString().padStart(2, '0'));
    $(".minutes").find("input").val(nmin.toString().padStart(2, '0'));
    // when reaching 00:00 stop repetition
    if (nsec == 0 && nmin == 0) {
      window.clearInterval(interv);
    };
  }, 1000);
}

$('.start').click(function(){
  // disable input first
  $(".minutes").find("input").prop("disabled", true);
  $(".seconds").find("input").prop("disabled", true);
  pomodoro();
});

// easy settings way, since we can just enable input
$(".settings").click(function(event){
  event.preventDefault();
  $(".minutes").find("input").prop("disabled", false);
  $(".seconds").find("input").prop("disabled", false);
})
